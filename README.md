# Code Karts 2

## Demo
- [https://codekarts.com/2/](https://codekarts.com/2/)


## Uitbreidingen
1. Schieten + health
- - Continue schieten, health gaat achteruit wanneer kart geraakt wordt
2. Scores > battle modus
3. Tegenstanders kunnen detecteren
4. Pickup op veld, collision zorgt ervoor dat ammo aangevuld wordt, beperkte ammo
5. Pickups kunnen detecteren
6. Verschillende pickups, schildpad (schild, ram), inktvis (gladde inkt straal), blowfish (obstakel, rollend obstakel, ontploft?)
7. Geupdate scores
8. Pickups wisselen op verschillende snelheid/volgorde, karts kunen dit zien
9. Nintendo quality pickup mascottes (schildpad, inktvis, blowfish)
10. Mini-competition between mascottes