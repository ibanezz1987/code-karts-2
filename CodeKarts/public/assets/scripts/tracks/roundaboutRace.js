const RoundaboutRace = class {
    name = 'Roundabout Race';
    type = 'race'; // race, battleRace, battle, captureTheFlag
    thumbnail = 'roundabout-race.jpg';

    /*
     * Add a name and colors to your kart
     */
    getSettings() {
        return {
            name: this.name,
            type: this.type,
            thumbnail: this.thumbnail,
            stage: {
                width: 6000,
                height: 6000
            },
            spawnBoxes: [
                {x: 2000, y: 100, width: 2000, height: 1800},     // Top
                {x: 4000, y: 2000, width: 1800, height: 2000},  // Right
                {x: 2000, y: 4000, width: 2000, height: 1800},  // Bottom
                {x: 100, y: 2000, width: 1800, height: 2000},     // Left
            ],
            checkpoints: [
                // Place checkpoints in multiple arrays to give checkpoints a certain order.
                // Place checkpoints in same array if the order between them is not important.
                // When using a finish, remember to always place it seperate in the last array.
                [
                    {
                        type: 'checkpoint',
                        startPoint: {x: 100, y: 3000},
                        endPoint: {x: 1900, y: 3000},
                    },
                ],
                [
                    {
                        type: 'checkpoint',
                        startPoint: {x: 3000, y: 4092},
                        endPoint: {x: 3000, y: 5884},
                    },
                ],
                [
                    {
                        type: 'checkpoint',
                        startPoint: {x: 4100, y: 3000},
                        endPoint: {x: 5900, y: 3000},
                    },
                ],
                [
                    {
                        type: 'finish',
                        startPoint: {x: 3000, y: 130},
                        endPoint: {x: 3000, y: 1900},
                    },
                ],
            ],
            layers: [
                {
                    type: 'grass',
                },
                {
                    type: 'track',
                    checkForCollisions: true,
                    borders: this.getPolygon(3000, 3000, 2900, 30),
                },
                {
                    type: 'grass',
                    checkForCollisions: true,
                    borders: this.getPolygon(3000, 3000, 1100, 26),
                },
                {
                    type: 'water',
                    borders: this.getPolygon(3000, 3000, 900, 20),
                },
            ],
            elements: {
                trees: this.getPolygon(3000, 3000, 900, 6).concat([{x: 5333, y: 5333}, {x: 667, y: 5333}, {x: 667, y: 667}, {x: 5333, y: 667}]),
            },
            trackProjectiles: {
                infiniteProjectiles: false,
                pickupZones: [
                    {x: 500, y: 3000},
                    {x: 1000, y: 3000},
                    {x: 1500, y: 3000},
                    {x: 4500, y: 3000},
                    {x: 5000, y: 3000},
                    {x: 5500, y: 3000},
                ],
                pickupTypes: [
                    {amount: 1, type: 'bullet'},
                    {amount: 3, type: 'bullet'},
                    {amount: 5, type: 'bullet'},
                ],
            },
        }
    }

    getPolygon(x, y, size, sides) {
        let polygon = [];

        for (var i = 0; i < sides; i++) {
            const pointX = x + size * Math.cos(i * 2 * Math.PI / sides);
            const pointY = y + size * Math.sin(i * 2 * Math.PI / sides);

            polygon.push({x: pointX, y: pointY});
        }

        return polygon;
    }
}