const FreeOrder = class {
    name = 'Free Order';
    type = 'race'; // race, battleRace, battle, captureTheFlag
    thumbnail = 'free-order.jpg';

    /*
     * Add a name and colors to your kart
     */
    getSettings() {
        return {
            name: this.name,
            type: this.type,
            thumbnail: this.thumbnail,
            stage: {
                width: 6000,
                height: 6000
            },
            spawnBoxes: [
                {x: 2200, y: 2200, width: 1600, height: 1600},
            ],
            totalLaps: 3,
            checkpoints: [
                // Place checkpoints in multiple arrays to give checkpoints a certain order.
                // Place checkpoints in same array if the order between them is not important.
                // When using a finish, remember to always place it seperate in the last array.
                [
                    {
                        type: 'checkpoint',
                        startPoint: {x: 305, y: 305},
                        endPoint: {x: 932, y: 932},
                    },
                    {
                        type: 'checkpoint',
                        startPoint: {x: 5695, y: 305},
                        endPoint: {x: 5068, y: 932},
                    },
                    {
                        type: 'checkpoint',
                        startPoint: {x: 305, y: 5695},
                        endPoint: {x: 932, y: 5068},
                    },
                    {
                        type: 'checkpoint',
                        startPoint: {x: 5695, y: 5695},
                        endPoint: {x: 5068, y: 5068},
                    },
                ],
            ],
            layers: [
                {
                    type: 'grass',
                },
                {
                    type: 'track',
                    checkForCollisions: true,
                    borders: [
                        {x: 100, y: 800},
                        {x: 153, y: 532},
                        {x: 305, y: 305},
                        {x: 532, y: 153},
                        {x: 800, y: 100},
                        {x: 5200, y: 100},
                        {x: 5468, y: 153},
                        {x: 5695, y: 305},
                        {x: 5847, y: 532},
                        {x: 5900, y: 800},
                        {x: 5900, y: 5200},
                        {x: 5847, y: 5468},
                        {x: 5695, y: 5695},
                        {x: 5468, y: 5847},
                        {x: 5200, y: 5900},
                        {x: 800, y: 5900},
                        {x: 532, y: 5847},
                        {x: 305, y: 5695},
                        {x: 153, y: 5468},
                        {x: 100, y: 5200},
                    ],
                },

                // Bars
                {
                    type: 'grass',
                    checkForCollisions: true,
                    borders: [
                        {x: 2900, y: 100},
                        {x: 2900, y: 1500},
                        {x: 3100, y: 1500},
                        {x: 3100, y: 100},
                    ],
                },
                {
                    type: 'grass',
                    checkForCollisions: true,
                    borders: [
                        {x: 100, y: 2900},
                        {x: 100, y: 3100},
                        {x: 1500, y: 3100},
                        {x: 1500, y: 2900},
                    ],
                },
                {
                    type: 'grass',
                    checkForCollisions: true,
                    borders: [
                        {x: 2900, y: 5900},
                        {x: 2900, y: 4500},
                        {x: 3100, y: 4500},
                        {x: 3100, y: 5900},
                    ],
                },
                {
                    type: 'grass',
                    checkForCollisions: true,
                    borders: [
                        {x: 5900, y: 2900},
                        {x: 5900, y: 3100},
                        {x: 4500, y: 3100},
                        {x: 4500, y: 2900},
                    ],
                },

                // Pillars
                {
                    type: 'grass',
                    checkForCollisions: true,
                    borders: this.getPolygon(1000, 1000, 100, 12),
                },
                {
                    type: 'grass',
                    checkForCollisions: true,
                    borders: this.getPolygon(5000, 1000, 100, 12),
                },
                {
                    type: 'grass',
                    checkForCollisions: true,
                    borders: this.getPolygon(1000, 5000, 100, 12),
                },
                {
                    type: 'grass',
                    checkForCollisions: true,
                    borders: this.getPolygon(5000, 5000, 100, 12),
                },

                // Spawnzone
                {
                    type: 'trackLight',
                    borders: this.getPolygon(3000, 3000, 800, 20),
                },
            ],
            elements: {
                // trees: this.getPolygon(3000, 3000, 900, 6).concat([{x: 5333, y: 5333}, {x: 667, y: 5333}, {x: 667, y: 667}, {x: 5333, y: 667}]),
            },
            trackProjectiles: {
                infiniteProjectiles: false,
                pickupZones: [
                    {x: 3000, y: 2750},
                    {x: 3000, y: 3250},
                    {x: 2750, y: 3000},
                    {x: 3250, y: 3000},
                ],
                pickupTypes: [
                    {amount: 1, type: 'bullet'},
                    {amount: 3, type: 'bullet'},
                    {amount: 5, type: 'bullet'},
                ],
            },
        }
    }

    getPolygon(x, y, size, sides) {
        let polygon = [];

        for (var i = 0; i < sides; i++) {
            const pointX = x + size * Math.cos(i * 2 * Math.PI / sides);
            const pointY = y + size * Math.sin(i * 2 * Math.PI / sides);

            polygon.push({x: Math.round(pointX), y: Math.round(pointY)});
        }

        return polygon;
    }
}