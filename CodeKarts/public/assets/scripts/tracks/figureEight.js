const FigureEight = class {
    name = 'Figure Eight';
    type = 'race'; // race, battleRace, battle, captureTheFlag
    thumbnail = 'figure-eight.jpg';

    /*
    * Add a name and colors to your kart
    */
    getSettings() {
        return {
            name: this.name,
            type: this.type,
            thumbnail: this.thumbnail,
            stage: {
                width: 7100,
                height: 4000
            },
            spawnBoxes: [
                {x: 2600, y: 1309, width: 1900, height: 1382},
            ],
            checkpoints: [
                // Place checkpoints in multiple arrays to give checkpoints a certain order.
                // Place checkpoints in same array if the order between them is not important.
                // When using a finish, remember to always place it seperate in the last array.
                [
                    {
                        type: 'checkpoint',
                        startPoint: {x: 5700, y: 2000},
                        endPoint: {x: 6800, y: 2000},
                    },
                ],
                [
                    {
                        type: 'checkpoint',
                        startPoint: {x: 5100, y: 309},
                        endPoint: {x: 5100, y: 1406},
                    },
                ],
                [
                    {
                        type: 'checkpoint',
                        startPoint: {x: 2000, y: 2594},
                        endPoint: {x: 2000, y: 3691},
                    },
                ],
                [
                    {
                        type: 'checkpoint',
                        startPoint: {x: 300, y: 2000},
                        endPoint: {x: 1400, y: 2000},
                    },
                ],
                [
                    {
                        type: 'checkpoint',
                        startPoint: {x: 2000, y: 309},
                        endPoint: {x: 2000, y: 1406},
                    },
                ],
                [
                    {
                        type: 'finish',
                        startPoint: {x: 5100, y: 2594},
                        endPoint: {x: 5100, y: 3691},
                    },
                ],
            ],
            layers: [
                {
                    type: 'grass',
                },
                {
                    type: 'track',
                    checkForCollisions: true,
                    borders: [
                        {x: 3375, y: 2999},
                        {x: 3138, y: 3263},
                        {x: 2850, y: 3472},
                        {x: 2525, y: 3617},
                        {x: 2178, y: 3691},
                        {x: 1822, y: 3691},
                        {x: 1475, y: 3617},
                        {x: 1150, y: 3472},
                        {x: 862, y: 3263},
                        {x: 625, y: 2999},
                        {x: 447, y: 2691},
                        {x: 337, y: 2353},
                        {x: 300, y: 2000},
                        {x: 337, y: 1647},
                        {x: 447, y: 1309},
                        {x: 625, y: 1001},
                        {x: 862, y: 737},
                        {x: 1150, y: 528},
                        {x: 1475, y: 383},
                        {x: 1822, y: 309},
                        {x: 2178, y: 309},
                        {x: 2525, y: 383},
                        {x: 2850, y: 528},
                        {x: 3138, y: 737},
                        {x: 3375, y: 1001},
                        {x: 3550, y: 1309},
                        {x: 3725, y: 1001},
                        {x: 3962, y: 737},
                        {x: 4250, y: 528},
                        {x: 4575, y: 383},
                        {x: 4922, y: 309},
                        {x: 5278, y: 309},
                        {x: 5625, y: 383},
                        {x: 5950, y: 528},
                        {x: 6238, y: 737},
                        {x: 6475, y: 1001},
                        {x: 6653, y: 1309},
                        {x: 6763, y: 1647},
                        {x: 6800, y: 2000},
                        {x: 6763, y: 2353},
                        {x: 6653, y: 2691},
                        {x: 6475, y: 2999},
                        {x: 6238, y: 3263},
                        {x: 5950, y: 3472},
                        {x: 5625, y: 3617},
                        {x: 5278, y: 3691},
                        {x: 4922, y: 3691},
                        {x: 4575, y: 3617},
                        {x: 4250, y: 3472},
                        {x: 3962, y: 3263},
                        {x: 3725, y: 2999},
                        {x: 3550, y: 2691},
                    ],
                },
                {
                    type: 'grass',
                    checkForCollisions: true,
                    borders: this.getPolygon(2000, 2000, 600, 22),
                },
                {
                    type: 'grass',
                    checkForCollisions: true,
                    borders: this.getPolygon(5100, 2000, 600, 22),
                },
            ],
            elements: {
                trees: [
                    {x: 2000, y: 2000},
                    {x: 5100, y: 2000},
                    {x: 3550, y: 509},
                    {x: 3550, y: 3591},
                    {x: 200, y: 1009},
                    {x: 200, y: 2991},
                    {x: 6900, y: 1009},
                    {x: 6900, y: 2991},
                ],
            },
        }
    }

    getPolygon(x, y, size, sides) {
        let polygon = [];

        for (var i = 0; i < sides; i++) {
            const pointX = x + size * Math.cos(i * 2 * Math.PI / sides);
            const pointY = y + size * Math.sin(i * 2 * Math.PI / sides);

            polygon.push({x: Math.round(pointX), y: Math.round(pointY)});
        }

        return polygon;
    }
}