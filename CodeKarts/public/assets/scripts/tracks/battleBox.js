const BattleBox = class {
    name = 'Battle Box';
    type = 'battle'; // race, battleRace, battle, captureTheFlag
    thumbnail = 'battle-box.jpg';

    /*
     * Add a name and colors to your kart
     */
    getSettings() {
        return {
            name: this.name,
            type: this.type,
            thumbnail: this.thumbnail,
            stage: {
                width: 4000,
                height: 4000
            },
            spawnBoxes: [{x: 400, y: 400, width: 3200, height: 3200}],
            layers: [
                {
                    type: 'grass',
                },
                {
                    type: 'track',
                    checkForCollisions: true,
                    borders: [{x: 400, y: 400}, {x: 3600, y: 400}, {x: 3600, y: 3600}, {x: 400, y: 3600}]
                },
                {
                    type: 'water',
                    borders: [{x: 3650, y: 0}, {x: 3850, y: 4000}, {x: 4000, y: 4000}, {x: 4000, y: 0}]
                },
            ],
            elements: {
                trees: [{x: 250, y: 400}, {x: 180, y: 1900}, {x: 230, y: 2700}, {x: 3600, y: 3900}, {x: 400, y: 150}],
            },
            trackProjectiles: {
                infiniteProjectiles: false,
                pickupZones: [
                    {x: 1650, y: 1650},
                    {x: 2350, y: 2350},
                    {x: 2350, y: 1650},
                    {x: 1650, y: 2350},
                ],
                pickupTypes: [
                    {amount: 1, type: 'bullet'},
                    {amount: 3, type: 'bullet'},
                    {amount: 5, type: 'bullet'},
                ],
            },
        }
    }

    getPolygon(x, y, size, sides) {
        let polygon = [];

        for (var i = 0; i < sides; i++) {
            const pointX = x + size * Math.cos(i * 2 * Math.PI / sides);
            const pointY = y + size * Math.sin(i * 2 * Math.PI / sides);

            polygon.push({x: pointX, y: pointY});
        }

        return polygon;
    }
}