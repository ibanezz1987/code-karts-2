const Manual = class extends Kart {
    /*
     * Add a name and colors to your kart
     */
    getAppearance() {
        return {
            name: 'Manual',
            colors: {
                kartPrimaryBodySides: '0x660000',
                kartPrimaryBodyFront: '0x660000',
                kartPrimaryBodySteer: '0x660000',
                kartSecondaryBodySeat: '0x111111',
                kartSecondaryBodyWing: '0x111111',
                driverPrimaryHelmet: '0xaaaaaa',
                driverPrimaryHands: '0xaaaaaa',
                driverSecondaryBody: '0x440000',
                driverTertiaryShoes: '0xaaaaaa',
                kartFramePrimary: '0x660000',
                kartFrameSecondary: '0x000000',
            }
        }
    }

    // Constructor gets called at the start of a game.
    // Don't remove the next 2 lines. The channel is required for communication with the game.
    constructor(channel) {
        super(channel);

        this.pressedKeys = [];

        document.addEventListener('keydown', event => {
            const keyIndex = this.pressedKeys.indexOf(event.code);
            if(keyIndex === -1) {
                this.pressedKeys.push(event.code);
            }
        });

        document.addEventListener('keyup', event => {
            const keyIndex = this.pressedKeys.indexOf(event.code);
            if(keyIndex !== -1) {
                this.pressedKeys.splice(keyIndex, 1);
            }
        });
    };

    // Everything within run gets called every tick.
    run() {
        if(this.pressedKeys.indexOf('KeyW') !== -1) {
            this.accelerate();
        }
        else if(this.pressedKeys.indexOf('KeyS') !== -1) {
            this.decelerate();
        }

        if(this.pressedKeys.indexOf('KeyD') !== -1) {
            this.turnRight();
        }
        else if(this.pressedKeys.indexOf('KeyA') !== -1) {
            this.turnLeft();
        }

        if(this.pressedKeys.indexOf('Space') !== -1 || this.pressedKeys.indexOf('ShiftLeft') !== -1 || this.pressedKeys.indexOf('ShiftRight') !== -1) {
            this.fire();
        }
    };
}