const Idle = class extends Kart {
    /*
     * Add a name and colors to your kart
     */
    getAppearance() {
        return {
            name: 'Idle',
            colors: {
                kartPrimaryBodySides: '0xdddddd',
                kartPrimaryBodyFront: '0xdddddd',
                kartPrimaryBodySteer: '0xdddddd',
                kartSecondaryBodySeat: '0x111111',
                kartSecondaryBodyWing: '0x111111',
                driverPrimaryHelmet: '0xdddddd',
                driverPrimaryHands: '0xdddddd',
                driverSecondaryBody: '0x333333',
                driverTertiaryShoes: '0xdddddd',
                kartFramePrimary: '0xbbbbbb',
                kartFrameSecondary: '0x000000',
            }
        }
    }

    onKartSpotted(event) {
        this.fire();
    }
}