const Accelerate = class extends Kart {
    /*
     * Add a name and colors to your kart
     */
    getAppearance() {
        const useLightColorScheme = this.channel.id % 2;
        const modifyColorIndex = this.channel.id % 3;
        const colorSchemePrimary = `0x${modifyColorIndex === 0 ? 'aa' : '00'}${modifyColorIndex === 1 ? 'aa' : '00'}${modifyColorIndex === 2 ? 'aa' : '00'}`;
        const colorSchemeSecondary = `0x${modifyColorIndex === 0 ? '77' : '00'}${modifyColorIndex === 1 ? '77' : '00'}${modifyColorIndex === 2 ? '77' : '00'}`;

        return {
            name: 'Accelerate',
            colors: {
                kartPrimaryBodySides: useLightColorScheme ? colorSchemeSecondary : '0x111111',
                kartPrimaryBodyFront: useLightColorScheme ? colorSchemeSecondary : '0x111111',
                kartPrimaryBodySteer: useLightColorScheme ? colorSchemeSecondary : '0x111111',
                kartSecondaryBodySeat: useLightColorScheme ? colorSchemeSecondary : '0x000000',
                kartSecondaryBodyWing: useLightColorScheme ? colorSchemeSecondary : '0x000000',
                driverPrimaryHelmet: useLightColorScheme ? '0xeeeeee' : colorSchemePrimary,
                driverPrimaryHands: useLightColorScheme ? '0xeeeeee' : colorSchemePrimary,
                driverSecondaryBody: useLightColorScheme ? colorSchemePrimary : '0x111111',
                driverTertiaryShoes: useLightColorScheme ? '0xeeeeee' : colorSchemePrimary,
                kartFramePrimary: useLightColorScheme ? colorSchemePrimary : '0x000000',
                kartFrameSecondary: useLightColorScheme ? '0xffffff' : colorSchemePrimary,
            }
        }
    }

    // Constructor gets called at the start of a game.
    // Don't remove the next 2 lines. The channel is required for communication with the game.
    constructor(channel) {
        super(channel);

        // Place to store variables. For example: this.isGoingForward = true;
        this.goForward = true;
        this.direction = true;

        window.setInterval(function() {
            this.direction = Math.round(Math.random() * 2);
        }.bind(this), 2500);

        window.setInterval(function() {
            this.goForward = Math.round(Math.random()) === 1;
        }.bind(this), 5000);

        // console.log(this.getRadius(), this.getX(), this.getY(), this.getVelocity(), this.getVelocityX(), this.getVelocityY(), this.getRotation(), this.getCurrentTick(), this.getStageWidth(), this.getStageHeight() );
    };

    // Everything within run gets called every tick.
    run() {
        if(this.goForward) {
            this.accelerate();
        }
        else {
            this.decelerate();
        }

        if(this.direction === 0) {
            this.turnRight();
        }
        else if(this.direction === 1) {
            this.turnLeft();
        }
    };

    onHitWall(event) {
        // console.log('onHitWall', event);
    }

    onHitKart(event) {
        // console.log('onHitKart', event);
    }

    onTakenDamage(event) {
        // console.log('onTakenDamage', event);
    }

    onTakenDamageByBullet(event) {
        // console.log('onTakenDamageByBullet', event);
    }

    onKartSpotted(event) {
        this.fire();
        // console.log('onKartSpotted', event);
    }
}