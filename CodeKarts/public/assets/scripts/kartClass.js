const Kart = class {
    /*
     * Add a name and colors to your kart
     */
    getAppearance() {
        return {
            name: 'Unnamed',
            colors: {
                driverPrimaryHands: '0xffffff',
                driverPrimaryHelmet: '0xffffff',
                driverSecondaryBody: '0xffffff',
                driverTertiaryShoes: '0xffffff',
                kartFramePrimary: '0xffffff',
                kartFrameSecondary: '0xffffff',
                kartPrimaryBodyFront: '0xffffff',
                kartPrimaryBodySides: '0xffffff',
                kartPrimaryBodySteer: '0xffffff',
                kartSecondaryBodySeat: '0xffffff',
                kartSecondaryBodyWing: '0xffffff',
            }
        }
    }

    constructor(channel) {
        this.channel = channel;
    };

    /*
     * Default empty run loop function
     */
    run() { };

    /*
     * Events
     * - Define all event methods, so that kartPeer can always call them
     */
    onHitWall(event) { }

    onHitKart(event) { }

    onTakenDamage(event) { }

    onTakenDamageByBullet(event) { }

    onKartSpotted(event) { }

    /*
     * Execute movement
     */
    accelerate() {
        this.channel.movement.push({type: 'accelerate'});
    };

    decelerate() {
        this.channel.movement.push({type: 'decelerate'});
    }

    turnLeft(maxChange) {
        this.channel.movement.push({type: 'turnLeft', value: maxChange});
    }

    turnRight(maxChange) {
        this.channel.movement.push({type: 'turnRight', value: maxChange});
    }

    /*
     * Execute firing
     */
    fire() {
        return this.channel.isFiring = true;
    }

    /*
     * Request kartPeer values
     */
    getRadius() {
        return this.channel.radius;
    }

    getX() {
        return this.channel.x;
    }

    getY() {
        return this.channel.y;
    }

    getVelocity() {
        return this.channel.velocity;
    }

    getVelocityX() {
        return this.channel.velocityX;
    }

    getVelocityY() {
        return this.channel.velocityY;
    }

    getRotation() {
        return this.channel.rotation;
    }

    // Request game values
    getCurrentTick() {
        return this.channel.currentTick;
    };

    getStageWidth() {
        return this.channel.stageWidth;
    };
    
    getStageHeight() {
        return this.channel.stageHeight;
    };
}
