import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { NavMainComponent } from './nav/nav-main/nav-main.component';

@Component({
  selector: 'app-root',
  imports: [RouterOutlet, NavMainComponent],
  templateUrl: './app.component.html',
})
export class AppComponent {
  title = 'code-karts';
}
