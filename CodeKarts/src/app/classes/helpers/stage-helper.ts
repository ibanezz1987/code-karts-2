import { Vector2 } from '../geometry/vector2';

export class StageHelper {
  getDistributedKartPositions(
    area: any,
    totalPositions: number,
    kartSize: number
  ): Vector2[] {
    let positions: Vector2[] = [];
    let attemptsLeft: number = totalPositions * 10;

    // Try to fill all positions within the accepted attemps
    while (positions.length < totalPositions && attemptsLeft > 0) {
      // Get a new random position
      const x: number = Math.round(
        (area.width - kartSize * 2) * Math.random() + kartSize + area.x
      );
      const y: number = Math.round(
        (area.height - kartSize * 2) * Math.random() + kartSize + area.y
      );
      let available: boolean = true;

      // Test if the random position is available
      for (let i = 0; i < positions.length; i++) {
        const position: Vector2 = positions[i];

        if (
          Math.abs(position.x - x) < kartSize &&
          Math.abs(position.y - y) < kartSize
        ) {
          available = false;
          break;
        }
      }

      // Add the new position
      if (available) {
        positions.push(new Vector2(x, y));
      }

      attemptsLeft -= 1;
    }

    // Return positions
    return positions;
  }
}
