import { Vector2 } from '../geometry/vector2';

export class CalculationsHelper {
  toRadians(degrees: number): number {
    return (degrees * Math.PI) / 180;
  }

  toDegrees(radians: number): number {
    return (180 / Math.PI) * radians;
  }

  normalRelativeAngle(angle: number): number {
    if (angle > -180 && angle <= 180) {
      return angle;
    }

    let fixedAngle = angle;

    while (fixedAngle <= -180) {
      fixedAngle += 360;
    }

    while (fixedAngle > 180) {
      fixedAngle -= 360;
    }

    return fixedAngle;
  }

  normalAbsoluteAngle(angle: number): number {
    let fixedAngle = angle;

    if (angle >= 0 && angle < 360) {
      return angle;
    }

    while (fixedAngle < 0) {
      fixedAngle += 360;
    }

    while (fixedAngle >= 360) {
      fixedAngle -= 360;
    }

    return fixedAngle;
  }

  findPointOnALine(
    lineStart: Vector2,
    lineEnd: Vector2,
    distance: number
  ): Vector2 {
    const xLength: number = lineEnd.x - lineStart.x;
    const yLength: number = lineEnd.y - lineStart.y;
    const hypotenuseLength: number = Math.sqrt(
      Math.pow(xLength, 2) + Math.pow(yLength, 2)
    );
    // Determine the ratio between they shortened value and the full hypotenuse.
    const ratio: number = distance / hypotenuseLength;
    const newXLen: number = xLength * ratio;
    const newYLen: number = yLength * ratio;

    // Fix: Never return a NaN
    if (ratio === Infinity) {
      return lineStart;
    }

    return new Vector2(lineStart.x + newXLen, lineStart.y + newYLen);
  }

  findNearestPointOnALine(
    point: Vector2,
    lineStart: Vector2,
    lineEnd: Vector2
  ): Vector2 {
    const atob: Vector2 = new Vector2(
      lineEnd.x - lineStart.x,
      lineEnd.y - lineStart.y
    );
    const atop: Vector2 = new Vector2(
      point.x - lineStart.x,
      point.y - lineStart.y
    );
    const len: number = atob.x * atob.x + atob.y * atob.y;
    const dot: number = atop.x * atob.x + atop.y * atob.y;
    const t: number = Math.min(1, Math.max(0, dot / len));

    return new Vector2(lineStart.x + atob.x * t, lineStart.y + atob.y * t);
  }

  getFacingDirection(rotation: number): number {
    // Facing direction is a number between 0 and 4 (0 & 4 = up, 1 = right, 2 down, 3 = left)
    let facingDirection = Math.round((4 / 360) * (rotation % 360));

    // Return facing direction and replace 4s with 0s for convenience
    return facingDirection === 4 ? 0 : facingDirection;
  }

  isReversing(velocity: Vector2, rotation: number): boolean {
    const facingDirection: number = this.getFacingDirection(rotation);
    let isReversing: boolean = false;

    switch (facingDirection) {
      // Facing up
      case 0:
        isReversing = velocity.y >= 0;
        break;

      // Facing right
      case 1:
        isReversing = velocity.x < 0;
        break;

      // Facing down
      case 2:
        isReversing = velocity.y < 0;
        break;

      // Facing left
      case 3:
        isReversing = velocity.x >= 0;
        break;
    }

    return isReversing;
  }

  getRandomizedArray(originalArray: any[]): any[] {
    const array = originalArray.slice();

    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }

    return array;
  }

  getSplittedArrays(originalArray: any[], arrayLength: number): any[][] {
    let arrays = [];

    for (let i = 0; i < originalArray.length; i += arrayLength) {
      arrays.push(originalArray.slice(i, i + arrayLength));
    }

    return arrays;
  }
}
