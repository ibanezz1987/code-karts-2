import { KartPeer } from './../kart/kart-peer';

export class GameHelper {
    getAliveKarts(kartPeers:KartPeer[]):number {
        let aliveKarts:number = 0;

        kartPeers.forEach(kartPeer => {
            if(kartPeer.isAlive) {
                aliveKarts++;
            }
        });

        return aliveKarts;
    }
}
