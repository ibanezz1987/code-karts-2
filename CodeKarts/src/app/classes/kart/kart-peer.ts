import { KartMove } from './../../interfaces/kart-move.type';
import { CalculationsHelper } from './../helpers/calculations-helper';
import { GameSettingsService } from './../../services/settings/game-settings.service';
import { Sight } from './sight';
import { Bullet } from './../../classes/projectiles/bullet';
import { Vector2 } from './../geometry/vector2';

// Intersection class
// https://github.com/davidfig/intersects
declare const Intersects: any;

export class KartPeer {
  kartInstance: any; // Players' kart class
  id: number;
  name: string = '';
  colors: any;
  position: Vector2;
  // origin: Vector2 = new Vector2(
  //   this.settings.kart.radius,
  //   this.settings.kart.radius
  // );
  origin: Vector2;
  velocity: Vector2 = new Vector2(0, 0);
  inputVelocity: number = 0;
  rotation: number = Math.round(360 * Math.random());
  tireRotation: number = 0;
  // defaultRotation: number = this.settings.kart.defaultRotation;
  defaultRotation: number;
  score: number = 0;
  survivalPlace: number = -1;
  isAlive: boolean = true;
  // health: number = this.settings.kart.initialHealth;
  health: number;
  sight: Sight;
  pickupZoneTime: number = 0;
  lastFireTime: number = 0;
  bullets: any[] = [];
  // totalBullets: number = this.settings.trackProjectiles.infiniteProjectiles
  //   ? Infinity
  //   : 0;
  totalBullets: number;
  // defaultProjectile = this.settings.trackProjectiles.default;
  currentLap: number = 1;
  // totalLaps: number = this.settings.totalLaps;
  totalLaps: number;
  currentCheckpointGroup: number = 0;
  currentCheckpoint: number = 0;
  // currentCheckpointGroupQueue: any[] = this.settings.checkpoints.length
  //   ? JSON.parse(JSON.stringify(this.settings.checkpoints[0]))
  //   : [];
  currentCheckpointGroupQueue: any[];
  lastCheckpoint: any = {};
  // totalCheckpoints: number = this.settings.getTotalCheckpoints();
  totalCheckpoints: number;
  causedError: boolean = false;
  calculations = new CalculationsHelper();
  channel: any = {
    // Communication with the kart instance
    id: null,
    x: null,
    y: null,
    velocity: 0,
    rotation: this.rotation,
    movement: [],
  };

  constructor(
    id: number,
    position: any,
    private settings: GameSettingsService
  ) {
    // Refactor for update from angular 8 to 19
    this.origin = new Vector2(
      this.settings.kart.radius,
      this.settings.kart.radius
    );
    this.defaultRotation = this.settings.kart.defaultRotation;
    this.health = this.settings.kart.initialHealth;
    this.totalBullets = this.settings.trackProjectiles.infiniteProjectiles
      ? Infinity
      : 0;
    this.totalLaps = this.settings.totalLaps;
    this.currentCheckpointGroupQueue = this.settings.checkpoints.length
      ? JSON.parse(JSON.stringify(this.settings.checkpoints[0]))
      : [];
    this.totalCheckpoints = this.settings.getTotalCheckpoints();

    this.id = id;
    this.position = position;
    this.sight = new Sight(this.settings, this);

    // Give the channel initial values
    this.updateChannel();
  }

  activate(KartInstance: any): void {
    this.kartInstance = new KartInstance(this.channel);
  }

  getAppearance(): void {
    const appearance: any = this.kartInstance.getAppearance();

    this.name = appearance.name;
    this.colors = appearance.colors;
  }

  run(): void {
    if (this.isAlive) {
      // Collect movement from kart
      this.kartInstance.run();

      // Save movement in class
      const movement = this.channel.movement;

      // Update kart params
      this.applyInputVelocity(movement);
      this.updateRotation(movement);
      this.updatePosition();
      this.executeFiring();

      // Update line of sight
      this.sight.calculatePoints();

      // Apply friction
      this.velocity.multiplyWith(this.settings.kart.friction);

      // Update the world information that the kart receives
      this.updateChannel();
    }
  }

  applyInputVelocity(movement: KartMove[]): void {
    const topSpeed: number = this.settings.kart.topSpeed;
    let acceleration: number = this.settings.kart.acceleration;

    if (movement.filter((m) => m.type === 'accelerate').length) {
      // Give acceleration a boost when reversing
      acceleration = this.inputVelocity < 0 ? acceleration * 2 : acceleration;

      // Increment acceleration
      this.inputVelocity += acceleration;
      this.inputVelocity =
        this.inputVelocity > topSpeed ? topSpeed : this.inputVelocity;
    } else if (movement.filter((m) => m.type === 'decelerate').length) {
      // Give acceleration a boost when reversing
      acceleration = this.inputVelocity > 0 ? acceleration * 2 : acceleration;

      // Increment acceleration
      this.inputVelocity -= acceleration;
      this.inputVelocity =
        this.inputVelocity < -topSpeed ? -topSpeed : this.inputVelocity;
    }

    // Relative angles starts at 3 o'clock (3 = 0 degrees, 6 = 90, 9 = 180, 12 = -90)
    const relativeAngle: number = this.calculations.normalRelativeAngle(
      this.rotation - 90
    );
    const inputVelocityX: number =
      this.inputVelocity * Math.cos(this.calculations.toRadians(relativeAngle));
    const inputVelocityY: number =
      this.inputVelocity * Math.sin(this.calculations.toRadians(relativeAngle));

    // Get the velocity for each axis
    this.velocity.x = this.velocity.x + inputVelocityX;
    this.velocity.y = this.velocity.y + inputVelocityY;
  }

  updateRotation(movement: KartMove[]): void {
    // Determine whether we go in reverse. Rotation flips when going reverse.
    const isReversing: boolean = this.calculations.isReversing(
      this.velocity,
      this.rotation
    );
    let inputRotation: number = 0;
    let rotationChange: number = 0;

    // Get the direction of rotation. Calculate the amount of rotation
    movement.forEach((move) => {
      if (move.type === 'turnLeft') {
        const maxRotation = move.value ? move.value : Infinity;
        inputRotation =
          this.defaultRotation < maxRotation
            ? this.defaultRotation
            : maxRotation;
        rotationChange = isReversing ? inputRotation : 0 - inputRotation;
      } else if (move.type === 'turnRight') {
        const maxRotation = move.value ? move.value : Infinity;
        inputRotation =
          this.defaultRotation < maxRotation
            ? this.defaultRotation
            : maxRotation;
        rotationChange = isReversing ? 0 - inputRotation : inputRotation;
      } else {
        rotationChange = 0;
      }
    });

    // Apply the new rotation
    const newRotation =
      this.rotation + rotationChange * (this.velocity.getTotal() / 2);
    this.rotation = this.calculations.normalAbsoluteAngle(newRotation);

    // Apply tire rotation
    const tireMultiplier = 2000;
    this.tireRotation = isReversing
      ? (0 - rotationChange) * tireMultiplier
      : rotationChange * tireMultiplier;
  }

  updatePosition(): void {
    this.position = this.position.add(
      this.velocity.multiply(this.settings.delta / 1000)
    );
  }

  executeFiring(): void {
    if (
      !this.channel.isFiring ||
      this.hasActiveRecoil() ||
      !this.totalBullets
    ) {
      return;
    }

    // Add a bullet
    const bullet: Bullet = new Bullet(this, this.settings);
    this.bullets.push(bullet);

    // Keep track of recoil times
    this.lastFireTime = this.settings.currentTick;

    // Lower the total bullets
    this.totalBullets--;
  }

  hasActiveRecoil(): boolean {
    return (
      this.settings.currentTick - this.lastFireTime <
      this.settings.projectiles.bullet.cooldown
    );
  }

  updateBullets(): void {
    // Update projectiles
    for (let i = 0; i < this.bullets.length; i++) {
      const bullet: Bullet = this.bullets[i];

      bullet.update();

      if (
        this.causedError ||
        bullet.remainingLifespan === 0 ||
        bullet.velocity.getTotal() <= 0
      ) {
        this.removeBullet(i);
      }
    }

    this.cleanBulletArray();
  }

  removeBullet(i: number): void {
    delete this.bullets[i];
  }

  // Remove empty slots from bullets array
  cleanBulletArray(): void {
    this.bullets = this.bullets.filter(function (bullet) {
      return bullet != null;
    });
  }

  updateChannel(): void {
    // Kart values
    this.channel.id = this.id;
    this.channel.radius = this.settings.kart.radius;
    this.channel.x = this.position.x;
    this.channel.y = this.position.y;
    this.channel.velocity = this.velocity.getTotal();
    this.channel.velocityX = this.velocity.x;
    this.channel.velocityY = this.velocity.y;
    this.channel.rotation = this.rotation;

    // Game values
    this.channel.currentTick = this.settings.currentTick;
    this.channel.stageWidth = this.settings.stage.width;
    this.channel.stageHeight = this.settings.stage.height;

    // Clear saved movement in preparation for next tick
    this.channel.movement = [];

    // Reset fire state
    this.channel.isFiring = false;
  }

  /**
   * Events
   */
  onHitWall(event: any): void {
    this.kartInstance.onHitWall(event);
  }

  onHitKart(event: any): void {
    this.kartInstance.onHitKart(event);
  }

  onTakenDamage(event: any): void {
    this.kartInstance.onTakenDamage(event);
  }

  onTakenDamageByBullet(event: any): void {
    this.kartInstance.onTakenDamageByBullet(event);
  }

  onKartSpotted(event: any): void {
    this.kartInstance.onKartSpotted(event);
  }
}
