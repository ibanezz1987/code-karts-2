import { GameSettingsService } from './../../services/settings/game-settings.service';
import { KartPeer } from './kart-peer';
import { CalculationsHelper } from './../helpers/calculations-helper';
import { Vector2 } from './../geometry/vector2';

export class Sight {
  // length:number = this.settings.kart.viewDistance;
  length: number;
  startPoint!: Vector2;
  endPoint: Vector2 = new Vector2();
  calculations = new CalculationsHelper();

  constructor(
    private settings: GameSettingsService,
    private kartPeer: KartPeer
  ) {
    // Refactor for update from angular 8 to 19
    this.length = this.settings.kart.viewDistance;

    this.calculatePoints();
  }

  calculatePoints() {
    this.getStartPoint();
    this.getEndPoint();
  }

  getStartPoint() {
    this.startPoint = this.kartPeer.position;
  }

  getEndPoint() {
    const relativeAngle: number = this.calculations.normalRelativeAngle(
      this.kartPeer.rotation - 90
    );
    const relativeEndPointX =
      this.length * Math.cos(this.calculations.toRadians(relativeAngle));
    const relativeEndPointY =
      this.length * Math.sin(this.calculations.toRadians(relativeAngle));
    const relativeEndPoint = new Vector2(relativeEndPointX, relativeEndPointY);
    const absoluteEndPoint = relativeEndPoint.add(this.kartPeer.position);

    this.endPoint.x = absoluteEndPoint.x;
    this.endPoint.y = absoluteEndPoint.y;
  }
}
