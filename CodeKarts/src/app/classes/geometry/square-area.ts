export class SquareArea {
    width:number = 0;
    height:number = 0;
    x:number = 0;
    y:number = 0;

    constructor(width:number, height:number, x?:number, y?:number) {
        this.width = typeof width !== 'undefined' ? width : 0;
        this.height = typeof height !== 'undefined' ? height : 0;
        this.x = typeof x !== 'undefined' ? x : 0;
        this.y = typeof y !== 'undefined' ? y : 0;
    }
}
