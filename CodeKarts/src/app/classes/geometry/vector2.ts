export class Vector2 {
  x: number = 0;
  y: number = 0;

  constructor(x?: number, y?: number) {
    this.x = typeof x !== 'undefined' ? x : 0;
    this.y = typeof y !== 'undefined' ? y : 0;
  }

  multiplyWith(v: any): any {
    if (v.constructor === Vector2) {
      this.x *= v.x;
      this.y *= v.y;
    } else if (v.constructor === Number) {
      this.x *= v as number;
      this.y *= v as number;
    }
    return this;
  }

  addTo(v: any): any {
    if (v.constructor === Vector2) {
      this.x += v.x;
      this.y += v.y;
    } else if (v.constructor === Number) {
      this.x += v as number;
      this.y += v as number;
    }
    return this;
  }

  add(v: any): any {
    var result = this.copy();
    return result.addTo(v);
  }

  multiply(v: any): any {
    var result = this.copy();
    return result.multiplyWith(v);
  }

  copy(): any {
    return new Vector2(this.x, this.y);
  }

  distanceFrom(obj: any): number {
    return Math.sqrt(
      (this.x - obj.x) * (this.x - obj.x) + (this.y - obj.y) * (this.y - obj.y)
    );
  }

  // a squared + b squared = c squared
  getTotal(): number {
    return Math.sqrt(
      Math.pow(Math.abs(this.x), 2) + Math.pow(Math.abs(this.y), 2)
    );
  }
}
