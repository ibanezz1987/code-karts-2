import { GameSettingsService } from './../../services/settings/game-settings.service';
import { KartPeer } from './../kart/kart-peer';
import { Vector2 } from './../geometry/vector2';
import { CalculationsHelper } from './../helpers/calculations-helper';

export class Bullet {
  position: Vector2;
  velocity: Vector2 = new Vector2();
  initialKartVelocity: Vector2;
  rotation: number;
  // remainingLifespan:number = this.settings.projectiles.bullet.lifespan;
  remainingLifespan: number;
  calculations = new CalculationsHelper();

  constructor(kartPeer: KartPeer, private settings: GameSettingsService) {
    // Refactor for update from angular 8 to 19
    this.remainingLifespan = this.settings.projectiles.bullet.lifespan;

    this.position = kartPeer.position;
    this.rotation = kartPeer.rotation;
    this.initialKartVelocity = kartPeer.velocity;

    this.getInitialVelocity();
  }

  getInitialVelocity(): void {
    const initialVelocity: number = this.settings.projectiles.bullet.speed;

    // Relative angles starts at 3 o'clock (3 = 0 degrees, 6 = 90, 9 = 180, 12 = -90)
    const relativeAngle: number = this.calculations.normalRelativeAngle(
      this.rotation - 90
    );

    // Get the velocity for each axis
    this.velocity.x =
      initialVelocity * Math.cos(this.calculations.toRadians(relativeAngle));
    this.velocity.y =
      initialVelocity * Math.sin(this.calculations.toRadians(relativeAngle));

    // Calculate the combined velocity of kart and bullet
    this.velocity = this.velocity.add(this.initialKartVelocity);
  }

  update(): void {
    this.position = this.position.add(
      this.velocity.multiply(this.settings.delta / 1000)
    );
    this.velocity.multiplyWith(this.settings.projectiles.bullet.friction);
    this.remainingLifespan -= 1;
  }
}
