import { Component } from '@angular/core';
import { GameSettingsService } from '../../services/settings/game-settings.service';
import { GameService } from '../../services/game/game.service';
import { StatisticsService } from '../../services/statistics/statistics.service';
import { KartScore } from '../../interfaces/kart-score.type';
import { KartVectorComponent } from './kart-vector/kart-vector.component';

@Component({
  standalone: true,
  imports: [KartVectorComponent],
  selector: 'app-nav-main',
  templateUrl: './nav-main.component.html',
})
export class NavMainComponent {
  gameSettings: GameSettingsService;
  gameS: GameService;
  statistics: StatisticsService;
  activeItem: string = 'current';
  isMenuOpen: boolean = false;
  isStatisticsOpen: boolean = false;
  newGameSettings: any = {};
  kartScores: KartScore[] = [];
  kartVectorUrl: string;

  constructor(
    gameSettings: GameSettingsService,
    gameS: GameService,
    statistics: StatisticsService
  ) {
    this.gameSettings = gameSettings;
    this.gameS = gameS;
    this.statistics = statistics;
    this.kartVectorUrl = gameSettings.baseUrl + gameSettings.kartVector;
  }

  onOpenMenu(): void {
    this.isMenuOpen = !this.isMenuOpen;
  }

  setActiveMenuItem(handle: string): void {
    this.activeItem = handle;
  }

  getActiveTrack(): any {
    return this.newGameSettings.track;
  }

  setActiveTrack(track: any): void {
    this.newGameSettings.track = track;
  }

  getTracks(): any[] {
    const tracks = this.gameSettings.tracks;

    // Set default track
    if (!this.newGameSettings.track) {
      this.setActiveTrack(this.gameSettings.track);
    }

    return tracks;
  }

  getTrackThumbnail(track: any): string {
    return `assets/scripts/tracks/${track.thumbnail}`;
  }

  startNewGame() {
    this.isMenuOpen = false;

    this.gameS.startNewGame(this.newGameSettings);
  }

  onOpenStatistics() {
    this.isStatisticsOpen = !this.isStatisticsOpen;
  }

  ngAfterContentChecked() {
    this.kartScores = this.statistics.kartScores;
  }
}
