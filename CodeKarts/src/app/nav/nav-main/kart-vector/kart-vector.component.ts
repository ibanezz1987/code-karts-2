import { Component, Input } from '@angular/core';

@Component({
  standalone: true,
  selector: 'app-kart-vector',
  templateUrl: './kart-vector.component.html',
})
export class KartVectorComponent {
  @Input() colors: any;

  getColor(key: string) {
    return this.colors[key] ? this.colors[key].replace('0x', '#') : '#ffffff';
  }
}
