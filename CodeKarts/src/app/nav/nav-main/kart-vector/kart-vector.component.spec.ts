import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KartVectorComponent } from './kart-vector.component';

describe('KartVectorComponent', () => {
  let component: KartVectorComponent;
  let fixture: ComponentFixture<KartVectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KartVectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KartVectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
