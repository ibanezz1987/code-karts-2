import { Vector2 } from '../classes/geometry/vector2';

export interface Layer {
    type:string,
    checkForCollisions?:boolean;
    borders?:Vector2[];
}
