export interface KartScore {
    id:number;
    position:number;
    laps:number;
    totalLaps:number;
    checkpoints:number;
    totalCheckpoints:number;
    name:string;
    score:number;
    health:number;
    bullets:number;
    survivalPlace:number;
    colors:number;
}
