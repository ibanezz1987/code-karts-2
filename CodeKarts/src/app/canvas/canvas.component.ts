import { Component, ElementRef, OnInit } from '@angular/core';
import { RenderService } from './../services/render/render.service';

@Component({
  selector: 'app-canvas',
  templateUrl: './canvas.component.html',
})
export class CanvasComponent implements OnInit {
  constructor(private elementRef: ElementRef, private renderS: RenderService) {}

  async ngOnInit() {
    await this.renderS.createCanvas();
    this.elementRef.nativeElement.appendChild(this.renderS.app.canvas);
  }
}
