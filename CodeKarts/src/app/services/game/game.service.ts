import { Injectable } from '@angular/core';
import { GameSettingsService } from '../settings/game-settings.service';
import { StatisticsService } from '../statistics/statistics.service';
import { CollisionsService } from '../collisions/collisions.service';
import { RenderService } from '../render/render.service';
import { Vector2 } from './../../classes/geometry/vector2';
import { GameHelper } from './../../classes/helpers/game-helper';
import { KartPeer } from './../../classes/kart/kart-peer';

@Injectable({
  providedIn: 'root',
})
export class GameService {
  kartPeers: KartPeer[] = [];
  lastTime: any;
  currentTime: any;
  finishedRound: boolean = false;
  gameHelper: GameHelper = new GameHelper();

  constructor(
    private settings: GameSettingsService,
    private statistics: StatisticsService,
    private collisionsS: CollisionsService,
    private renderS: RenderService
  ) {
    // Fix kart position bug when browser tab is not active.
    window.addEventListener('blur', () => {
      if (!this.settings.isPaused) {
        this.togglePaused();
      }
    });
  }

  startNewGame(settings?: any) {
    // Set game values
    this.settings.applyDefaultSettings();

    // Switch track
    if (settings && settings.track) {
      this.settings.applyChosenTrackSettings(settings.track);
    }

    // Set view
    this.renderS.renderNewGame();

    // update kartPeers
    this.removeKarts();
    this.addKarts();

    // Reset scores
    this.statistics.resetScores();
    this.statistics.resetStatistics();

    // TODO: Countdown
    // Start game loop
    setTimeout(() => {
      this.startGame();
    }, 500);
  }

  removeKarts(): void {
    this.kartPeers = [];
  }

  addKarts(): void {
    const selectedKarts: any[] = this.settings.getSelectedKarts();
    const positions: Vector2[] = this.settings.getKartPositions();
    let index: number = 0;

    selectedKarts.forEach((Kart) => {
      const kartPeer: KartPeer = new KartPeer(
        index,
        positions[index],
        this.settings
      );

      kartPeer.activate(Kart);
      kartPeer.getAppearance();
      this.renderS.addKart(kartPeer);
      this.kartPeers.push(kartPeer);

      index++;
    });
  }

  stopGame(): void {
    this.settings.isActive = false;
  }

  togglePaused(): boolean {
    this.settings.isPaused = !this.settings.isPaused;

    // Restart game
    if (!this.settings.isPaused) {
      this.startGame();
    }

    return this.settings.isPaused;
  }

  startGame(): void {
    this.lastTime = Date.now();
    this.gameLoop();
  }

  gameLoop(): void {
    // Fix kart position bug when browser tab is not active.
    if (!this.settings.isActive || this.settings.isPaused) {
      return;
    }

    if (this.settings.isActive && !this.settings.isPaused) {
      window.requestAnimationFrame(() => this.gameLoop());
    }

    // Time/ticks
    this.currentTime = Date.now();
    this.settings.delta = this.currentTime - this.lastTime;

    if (this.settings.delta > this.settings.interval) {
      // Karts
      this.kartPeers.forEach((kartPeer) => {
        // Run kart for one tick
        if (!kartPeer.causedError && kartPeer.isAlive) {
          kartPeer.run();
        }

        // Update projectiles
        kartPeer.updateBullets();

        // Update renderer
        if (!kartPeer.causedError && kartPeer.isAlive) {
          this.renderS.updateKart(kartPeer);

          if (this.finishedRound) {
            kartPeer.survivalPlace = 1;
          }
        } else {
          this.renderS.updateDeadKart(kartPeer);
        }
      });

      // Update renderer
      this.renderS.updateTrack();

      // Time/ticks
      this.settings.currentTick++;
      this.lastTime = Date.now();

      // Collisions
      this.collisionsS.searchCollisions(this.kartPeers);

      // Statistics
      this.statistics.updateScores(this.kartPeers, this.settings.type);
      this.statistics.updateStatistics(this.kartPeers);

      // End of round
      if (
        this.gameHelper.getAliveKarts(this.kartPeers) <= 1 &&
        !this.finishedRound
      ) {
        // Mark round as ended
        this.finishedRound = true;
      }
    }
  }
}
