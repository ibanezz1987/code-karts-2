import { Injectable } from '@angular/core';
import { StageHelper } from './../../classes/helpers/stage-helper';
import { SquareArea } from './../../classes/geometry/square-area';
import { Vector2 } from './../../classes/geometry/vector2';
import { CalculationsHelper } from './../../classes/helpers/calculations-helper';
import { Layer } from '../../interfaces/layer.type';

// Load premade karts
// Refactor for update from angular 8 to 19
// @ts-ignore Allow function declaration
declare function Manual(channel): void;
// @ts-ignore Allow function declaration
declare function Accelerate(channel): void;
// @ts-ignore Allow function declaration
declare function Idle(channel): void;

// Load premade tracks
declare function BattleBox(): void;
declare function RoundaboutRace(): void;
declare function FigureEight(): void;
declare function FreeOrder(): void;

@Injectable({
  providedIn: 'root',
})
export class GameSettingsService {
  private calculations: CalculationsHelper = new CalculationsHelper();

  defaultSettings: any = {
    general: {
      framerate: 60,
      interval: 1000 / 60,
      delta: 1000 / 60,
      currentTick: 0,
      isActive: true,
      isPaused: false,
      tracks: [
        // Refactor for update from angular 8 to 19
        // @ts-ignore Allow function declaration
        new BattleBox(),
        // @ts-ignore Allow function declaration
        new RoundaboutRace(),
        // @ts-ignore Allow function declaration
        new FigureEight(),
        // @ts-ignore Allow function declaration
        new FreeOrder(),
      ],
    },
    colors: {
      colorScore: 0xffff00,
      colorScoreBG: 0x666600,
      colorHealth: 0x00ff00,
      colorHealthBG: 0xff0000,
      colorPickups: 0x00bcd4,
      colorPickupsBG: 0x005965,
      colorCheckPoints: 0x7f8284,
      colorFinish: 0x9da0a2,
    },
    images: {
      baseUrl: 'assets/images/',
      kartVector: 'kart/kart.svg',
    },
    physics: {
      bounceSlowDown: 0.9,
      bounceJump: 2,
      kart: {
        viewDistance: 2000,
        radius: 150,
        defaultRotation: 0.01,
        acceleration: 1.0,
        friction: 0.92,
        topSpeed: 80,
        initialHealth: 3,
      },
      projectiles: {
        pickupZone: {
          radius: 150,
          cooldown: 150,
          bullet: {
            max: 10,
          },
        },
        bullet: {
          damage: 1,
          cooldown: 30,
          speed: 500,
          friction: 0.995,
          radius: 2,
          lifespan: 100,
        },
      },
    },
    karts: {
      loadedKarts: [Manual, Accelerate, Idle],
      // selectedKarts: [0, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 2,],
      selectedKarts: [0, 1, 1, 1, 1, 1, 1, 1, 1, 2],
      // selectedKarts: [0, 2, 2],
      // selectedKarts: [0],
      // selectedKarts: [],
      kartPositions: [],
    },
    track: {
      // type: 'battle', // race, battleRace, battle, captureTheFlag
      // track: new BattleBox(),
      type: 'race', // race, battleRace, battle, captureTheFlag
      // Refactor for update from angular 8 to 19
      // @ts-ignore Allow function declaration
      track: new FreeOrder(),
      stage: {
        width: 3000,
        height: 3000,
      },
      spawnBoxes: [],
      totalLaps: 3,
      checkpoints: [[]],
      layers: [],
      elements: {
        trees: [],
      },
      trackProjectiles: {
        default: 'bullet',
        infiniteProjectiles: true,
        pickupZones: [],
        pickupTypes: [],
      },
    },
  };

  // General settings
  framerate!: number;
  interval!: number;
  delta!: number;
  currentTick!: number;
  isActive!: boolean;
  isPaused!: boolean;

  // Color settings
  colorScore: any;
  colorScoreBG: any;
  colorHealth: any;
  colorHealthBG: any;
  colorPickups: any;
  colorPickupsBG: any;
  colorCheckPoints: any;
  colorFinish: any;

  // Image settings
  baseUrl!: string;
  kartVector!: string;

  // Physics settings
  bounceSlowDown!: number;
  bounceJump!: number;
  kart: any;
  projectiles: any;

  // Karts settings
  loadedKarts!: any[];
  selectedKarts!: number[];
  kartPositions!: Vector2[];

  // Track settings
  type!: string;
  tracks: any;
  track: any;
  stage!: SquareArea;
  spawnBoxes!: SquareArea[];
  totalLaps!: number;
  checkpoints!: any[][];
  layers!: Layer[];
  elements: any;
  trackProjectiles: any;

  constructor() {
    this.applyDefaultSettings();
  }

  applyDefaultSettings(): void {
    // Apply general settings
    for (const key in this.defaultSettings.general) {
      // Refactor for update from angular 8 to 19
      // @ts-ignore: Keep this indexing logic
      this[key] = this.defaultSettings.general[key];
    }

    // Apply color settings
    for (const key in this.defaultSettings.colors) {
      // Refactor for update from angular 8 to 19
      // @ts-ignore: Keep this indexing logic
      this[key] = this.defaultSettings.colors[key];
    }

    // Apply image settings
    for (const key in this.defaultSettings.images) {
      // Refactor for update from angular 8 to 19
      // @ts-ignore: Keep this indexing logic
      this[key] = this.defaultSettings.images[key];
    }

    // Apply physics settings
    for (const key in this.defaultSettings.physics) {
      // Refactor for update from angular 8 to 19
      // @ts-ignore: Keep this indexing logic
      this[key] = this.defaultSettings.physics[key];
    }

    // Apply karts settings
    for (const key in this.defaultSettings.karts) {
      // Refactor for update from angular 8 to 19
      // @ts-ignore: Keep this indexing logic
      this[key] = this.defaultSettings.karts[key];
    }

    // Apply track settings
    for (const key in this.defaultSettings.track) {
      // Refactor for update from angular 8 to 19
      // @ts-ignore: Keep this indexing logic
      this[key] = this.defaultSettings.track[key];
    }

    // Apply settings in chosen track
    this.applyChosenTrackSettings();
  }

  applyChosenTrackSettings(chosenTrack?: any): void {
    const track = chosenTrack ? chosenTrack : this.track;
    const settings = track.getSettings ? track.getSettings() : undefined;

    if (!settings) {
      return;
    }

    // Loop through settings of a track, but only settings that are allowed for a track
    for (const key in settings) {
      if (key in this.defaultSettings.track) {
        // Refactor for update from angular 8 to 19
        // @ts-ignore: Keep this indexing logic
        this[key] = settings[key];
      }
    }

    // Reset possible track values from the previous track
    for (const key in this.defaultSettings.track) {
      if (!(key in settings)) {
        // Refactor for update from angular 8 to 19
        // @ts-ignore: Keep this indexing logic
        this[key] = this.defaultSettings.track[key];
      }
    }
  }

  updateStageSize(width: number, height: number): void {
    this.stage.width = width;
    this.stage.height = height;
  }

  updateFramerate(rate: number): void {
    this.framerate = rate;
    this.interval = 1000 / this.framerate;
    this.delta = 1000 / this.framerate;
  }

  getKartPositions(): Vector2[] {
    // Check if positions have been set by the track
    if (this.kartPositions.length) {
      // Return positions
      return this.kartPositions;
    }

    // If no spawn boxes are given then use the whole stage as a spawn box
    if (!this.spawnBoxes.length) {
      this.spawnBoxes = [
        { x: 0, y: 0, width: this.stage.width, height: this.stage.height },
      ];
    }

    // Randomize kart order + split kart into multiple arrays
    const randomizedKarts = this.calculations.getRandomizedArray(
      this.selectedKarts
    );
    const kartArrays = this.calculations.getSplittedArrays(
      randomizedKarts,
      Math.ceil(randomizedKarts.length / this.spawnBoxes.length)
    );
    let index = 0;

    this.spawnBoxes.forEach((box) => {
      const boxKarts = kartArrays[index];
      let generatedPositions: Vector2[] = [];

      if (boxKarts) {
        // Try to get distributed positions
        generatedPositions = new StageHelper().getDistributedKartPositions(
          box,
          boxKarts.length,
          this.kart.radius * 2
        );

        // Get random positions if distributed positions could not be generated
        if (generatedPositions.length < boxKarts.length) {
          generatedPositions = [];
          boxKarts.forEach(() => {
            const x: number = Math.round(
              (box.width - this.kart.radius * 2) * Math.random() +
                this.kart.radius +
                box.x
            );
            const y: number = Math.round(
              (box.height - this.kart.radius * 2) * Math.random() +
                this.kart.radius +
                box.y
            );

            generatedPositions.push(new Vector2(x, y));
          });
        }

        this.kartPositions = this.kartPositions.concat(generatedPositions);

        index++;
      }
    });

    // Return positions
    return this.kartPositions;
  }

  getSelectedKarts(): any[] {
    const karts: any[] = [];

    this.selectedKarts.forEach((id) => {
      const Kart = this.loadedKarts[id];

      // TODO: Add validation like here:
      //       https://bitbucket.org/ibanezz1987/code-karts/src/master/scripts/game/battle/battle.js

      karts.push(Kart);
    });

    return karts;
  }

  getTotalCheckpoints(): number {
    let totalCheckPoints: number = 0;

    this.checkpoints.forEach((checkpointOrder) => {
      checkpointOrder.forEach(() => {
        totalCheckPoints++;
      });
    });

    return totalCheckPoints;
  }
}
