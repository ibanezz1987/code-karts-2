import { Injectable } from '@angular/core';
import { GameSettingsService } from '../settings/game-settings.service';
import { CalculationsHelper } from './../../classes/helpers/calculations-helper';
import { GameHelper } from './../../classes/helpers/game-helper';
import { KartPeer } from './../../classes/kart/kart-peer';
import { Sight } from './../../classes/kart/sight';
import { Vector2 } from './../../classes/geometry/vector2';
import { Bullet } from './../../classes/projectiles/bullet';

// Intersection class
// https://github.com/davidfig/intersects
declare const Intersects: any;

@Injectable({
  providedIn: 'root',
})
export class CollisionsService {
  calculations: CalculationsHelper = new CalculationsHelper();
  gameHelper: GameHelper = new GameHelper();

  constructor(private settings: GameSettingsService) {}

  searchCollisions(kartPeers: KartPeer[]): void {
    for (let i = 0; i < kartPeers.length; i++) {
      const currentKart = kartPeers[i];

      if (currentKart.isAlive) {
        // Search border collisions
        this.searchBorderCollisions(currentKart);

        // Search checkpoint collisions
        this.searchCheckpointCollisions(currentKart);

        // Search pickup zone collisions
        this.searchPickupZoneCollisions(currentKart);
      }

      for (let j = 0; j < kartPeers.length; j++) {
        const otherKart = kartPeers[j];

        // Search bullet collisions
        if (i !== j && otherKart.isAlive) {
          this.searchBulletCollisions(kartPeers, currentKart, otherKart);
        }

        if (currentKart.isAlive && otherKart.isAlive) {
          // Check if other karts pass line of sight
          if (i !== j) {
            this.isKartWithinSight(currentKart, otherKart);
          }

          // Search kart collisions
          if (i > j) {
            this.searchKartCollisions(currentKart, otherKart);
          }
        }
      }
    }
  }

  searchBorderCollisions(currentKart: KartPeer): void {
    const stage = this.settings.stage;
    let defaultBorders: any[] = [
      { x: 0, y: 0 },
      { x: stage.width, y: 0 },
      { x: stage.width, y: stage.height },
      { x: 0, y: stage.height },
    ];
    let borderGroups: any[] = [defaultBorders];

    this.settings.layers.forEach((layer) => {
      if (layer.borders && layer.checkForCollisions) {
        borderGroups.push(layer.borders);
      }
    });

    borderGroups.forEach((borders) => {
      for (let i = 0; i < borders.length; i++) {
        const border1: any = borders[i];
        const border2: any = borders[i === borders.length - 1 ? 0 : i + 1];
        const isColliding: boolean = Intersects.circleLine(
          currentKart.position.x,
          currentKart.position.y,
          this.settings.kart.radius,
          border1.x,
          border1.y,
          border2.x,
          border2.y
        );

        if (isColliding) {
          this.handleBorderCollisions(currentKart, border1, border2);
        }
      }
    });
  }

  searchCheckpointCollisions(currentKart: KartPeer): void {
    if (
      this.settings.checkpoints.length === 0 ||
      (this.settings.checkpoints.length === 1 &&
        this.settings.checkpoints[0].length === 0)
    ) {
      return;
    }

    // Search for collision within current queue
    for (let i = 0; i < currentKart.currentCheckpointGroupQueue.length; i++) {
      const checkpoint = currentKart.currentCheckpointGroupQueue[i];
      const isColliding: boolean = Intersects.circleLine(
        currentKart.position.x,
        currentKart.position.y,
        this.settings.kart.radius,
        checkpoint.startPoint.x,
        checkpoint.startPoint.y,
        checkpoint.endPoint.x,
        checkpoint.endPoint.y
      );
      const start = checkpoint.startPoint;
      const end = checkpoint.endPoint;
      const lastStart = currentKart.lastCheckpoint.startPoint;
      const lastEnd = currentKart.lastCheckpoint.endPoint;
      const isNotLastCheckpoint =
        !lastStart ||
        !lastEnd ||
        ((start.x !== lastStart.x || start.y !== lastStart.y) &&
          (end.x !== lastEnd.x || end.y !== lastEnd.y));

      if (isColliding && isNotLastCheckpoint) {
        currentKart.lastCheckpoint = checkpoint;

        this.handleCheckpointCollisions(currentKart, i);
      }
    }
  }

  handleCheckpointCollisions(currentKart: KartPeer, checkpointIndex: number) {
    // Increase currentCheckpoint. Used to display current checkpoint of this lap
    currentKart.currentCheckpoint++;

    // Remove the checkpoint from te queue
    currentKart.currentCheckpointGroupQueue.splice(checkpointIndex, 1);

    // Increment checkpoint group
    if (currentKart.currentCheckpointGroupQueue.length === 0) {
      currentKart.currentCheckpointGroup++;

      // Increment lap
      if (
        currentKart.currentCheckpointGroup === this.settings.checkpoints.length
      ) {
        currentKart.currentCheckpointGroup = 0;
        currentKart.currentCheckpoint = 0;
        currentKart.currentLap++;
      }

      currentKart.currentCheckpointGroupQueue = JSON.parse(
        JSON.stringify(
          this.settings.checkpoints[currentKart.currentCheckpointGroup]
        )
      );
    }
  }

  handleBorderCollisions(
    currentKart: KartPeer,
    borderStart: Vector2,
    borderEnd: Vector2
  ): void {
    // Double the karts own power
    let power: number =
      (Math.abs(currentKart.velocity.x) + Math.abs(currentKart.velocity.y)) * 2;
    power = power * 0.00482;

    const nearestPoint: Vector2 = this.calculations.findNearestPointOnALine(
      currentKart.position,
      borderStart,
      borderEnd
    );
    const opposite: number = currentKart.position.y - nearestPoint.y;
    const adjacent: number = currentKart.position.x - nearestPoint.x;
    const rotation: number = Math.atan2(opposite, adjacent);

    const VelocityChange: Vector2 = new Vector2(
      90 * Math.cos(rotation) * power,
      90 * Math.sin(rotation) * power
    );
    currentKart.velocity = currentKart.velocity.addTo(VelocityChange);
    currentKart.velocity.multiplyWith(0.97);
    currentKart.position = this.calculations.findPointOnALine(
      currentKart.position,
      nearestPoint,
      -this.settings.bounceJump * 2
    );

    // Events
    // TODO: add actual bearing
    currentKart.onHitWall({ bearing: 0 });
  }

  searchPickupZoneCollisions(currentKart: KartPeer): void {
    const pickupZones: any[] = this.settings.trackProjectiles.pickupZones;

    pickupZones.forEach((zone) => {
      const isColliding: boolean = Intersects.circleCircle(
        currentKart.position.x,
        currentKart.position.y,
        this.settings.kart.radius,
        zone.x,
        zone.y,
        this.settings.projectiles.pickupZone.radius
      );

      if (isColliding) {
        this.handlePickupZoneCollisions(currentKart);
      }
    });
  }

  handlePickupZoneCollisions(currentKart: KartPeer): void {
    // Cooldown time
    if (this.hasKartActivePickupZoneCooldown(currentKart)) {
      return;
    }

    // Increase amount of bullets
    // TODO: add actual number of additional bullets
    // TODO: add visual representation of additional bullets
    // TODO: add getTotalBullets() to kart
    currentKart.totalBullets += 5;

    // Limit max bullets
    if (
      currentKart.totalBullets > this.settings.projectiles.pickupZone.bullet.max
    ) {
      currentKart.totalBullets =
        this.settings.projectiles.pickupZone.bullet.max;
    }

    // Keep track of cooldown times
    currentKart.pickupZoneTime = this.settings.currentTick;

    // Events
    // TODO: add event
    // TODO: add sight event for pickupZone
  }

  hasKartActivePickupZoneCooldown(currentKart: KartPeer): boolean {
    return (
      this.settings.currentTick - currentKart.pickupZoneTime <
      this.settings.projectiles.pickupZone.cooldown
    );
  }

  searchBulletCollisions(
    kartPeers: KartPeer[],
    currentKart: KartPeer,
    otherKart: KartPeer
  ): void {
    for (let k = 0; k < currentKart.bullets.length; k++) {
      const bullet: Bullet = currentKart.bullets[k];
      const isColliding: boolean = Intersects.circleCircle(
        bullet.position.x,
        bullet.position.y,
        this.settings.projectiles.bullet.radius,
        otherKart.position.x,
        otherKart.position.y,
        this.settings.kart.radius
      );

      if (isColliding) {
        this.handleBulletCollision(kartPeers, currentKart, otherKart, k);
      }
    }
  }

  handleBulletCollision(
    kartPeers: KartPeer[],
    currentKart: KartPeer,
    otherKart: KartPeer,
    bulletIndex: number
  ): void {
    const damage = this.settings.projectiles.bullet.damage;

    // Lower health
    otherKart.health -= damage;

    // Kill other kart
    if (otherKart.health <= 0) {
      otherKart.survivalPlace = this.gameHelper.getAliveKarts(kartPeers);
      otherKart.health = 0;
      otherKart.isAlive = false;
    }

    // Events
    // TODO: add actual bearing
    otherKart.onTakenDamage({ bearing: 0 });
    otherKart.onTakenDamageByBullet({ bearing: 0 });

    // Remove bullet
    currentKart.removeBullet(bulletIndex);
    currentKart.cleanBulletArray();

    // Increase score
    currentKart.score += damage;
  }

  searchKartCollisions(currentKart: KartPeer, otherKart: KartPeer): void {
    const currentKartNewPos: Vector2 = currentKart.position.add(
      currentKart.velocity.multiply(this.settings.delta / 1000)
    );
    const otherKartNewPos: Vector2 = otherKart.position.add(
      otherKart.velocity.multiply(this.settings.delta / 1000)
    );
    const distance: number = currentKartNewPos.distanceFrom(otherKartNewPos);
    const isColliding: boolean = distance < this.settings.kart.radius * 2;

    if (isColliding) {
      this.handleKartCollision(currentKart, otherKart);
    }
  }

  handleKartCollision(currentKart: KartPeer, otherKart: KartPeer): void {
    let power: number =
      Math.abs(currentKart.velocity.x) +
      Math.abs(currentKart.velocity.y) +
      (Math.abs(otherKart.velocity.x) + Math.abs(otherKart.velocity.y));
    power = power * 0.00482;

    const opposite: number = currentKart.position.y - otherKart.position.y;
    const adjacent: number = currentKart.position.x - otherKart.position.x;
    const rotation: number = Math.atan2(opposite, adjacent);

    const velocity2: Vector2 = new Vector2(
      90 * Math.cos(rotation + Math.PI) * power,
      90 * Math.sin(rotation + Math.PI) * power
    );
    otherKart.velocity = otherKart.velocity.addTo(velocity2);
    otherKart.velocity.multiplyWith(0.97);

    const velocity1: Vector2 = new Vector2(
      90 * Math.cos(rotation) * power,
      90 * Math.sin(rotation) * power
    );
    currentKart.velocity = currentKart.velocity.addTo(velocity1);
    currentKart.velocity.multiplyWith(0.97);

    // Events
    // TODO: add actual bearing
    currentKart.onHitKart({ bearing: 0 });
    otherKart.onHitKart({ bearing: 0 });
  }

  isKartWithinSight(currentKart: KartPeer, otherKart: KartPeer): void {
    const sight: Sight = currentKart.sight;
    const isColliding: boolean = Intersects.circleLine(
      otherKart.position.x,
      otherKart.position.y,
      this.settings.kart.radius,
      sight.startPoint.x,
      sight.startPoint.y,
      sight.endPoint.x,
      sight.endPoint.y
    );

    if (isColliding) {
      this.handleKartWithinSight(currentKart, otherKart);
    }
  }

  handleKartWithinSight(currentKart: KartPeer, otherKart: KartPeer): void {
    // Events
    // TODO: add actual bearing, distance
    currentKart.onKartSpotted({
      bearing: 0,
      distance: 0,
      id: otherKart.id,
      health: otherKart.health,
    });
  }
}
