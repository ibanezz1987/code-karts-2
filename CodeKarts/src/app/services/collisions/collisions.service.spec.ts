import { TestBed } from '@angular/core/testing';

import { CollisionsService } from './collisions.service';

describe('CollisionsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CollisionsService = TestBed.get(CollisionsService);
    expect(service).toBeTruthy();
  });
});
