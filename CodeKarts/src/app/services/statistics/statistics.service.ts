import { Injectable } from '@angular/core';
import { KartScore } from '../../interfaces/kart-score.type';
import { KartPeer } from '../../classes/kart/kart-peer';

@Injectable({
  providedIn: 'root',
})
export class StatisticsService {
  kartScores: KartScore[] = [];
  scoreHigh: number = 0;
  healthHigh: number = 0;
  bulletsHigh: number = 0;

  resetScores(): void {
    this.kartScores = [];
  }

  resetStatistics(): void {
    this.scoreHigh = 0;
    this.healthHigh = 0;
    this.bulletsHigh = 0;
  }

  updateScores(kartPeers: KartPeer[], raceType: string): void {
    const orderBattle = ['score', 'health', 'totalBullets'];
    const orderRace = ['currentLap', 'currentCheckpoint'];
    const order = raceType === 'battle' ? orderBattle : orderRace;

    kartPeers.sort(function (a, b) {
      if (
        // @ts-ignore: ignore TS warnings to allow Refactor for update from angular 8 to 19
        b[order[0]] === a[order[0]] &&
        // @ts-ignore: ignore TS warnings to allow Refactor for update from angular 8 to 19
        b[order[1]] === a[order[1]] &&
        order[2]
      ) {
        // @ts-ignore: ignore TS warnings to allow Refactor for update from angular 8 to 19
        return b[order[2]] - a[order[2]];
        // @ts-ignore: ignore TS warnings to allow Refactor for update from angular 8 to 19
      } else if (b[order[0]] === a[order[0]]) {
        // @ts-ignore: ignore TS warnings to allow Refactor for update from angular 8 to 19
        return b[order[1]] - a[order[1]];
      }
      // @ts-ignore: ignore TS warnings to allow Refactor for update from angular 8 to 19
      return b[order[0]] - a[order[0]];
    });

    this.kartScores = kartPeers.map((kartPeer, index) => {
      return {
        id: kartPeer.id,
        position: index + 1,
        laps: kartPeer.currentLap,
        totalLaps: kartPeer.totalLaps,
        checkpoints: kartPeer.currentCheckpoint,
        totalCheckpoints: kartPeer.totalCheckpoints,
        name: kartPeer.name,
        score: kartPeer.score,
        health: kartPeer.health,
        bullets: kartPeer.totalBullets,
        survivalPlace: kartPeer.survivalPlace,
        colors: kartPeer.colors,
      };
    });
  }

  updateStatistics(kartPeers: KartPeer[]): void {
    this.resetStatistics();

    kartPeers.forEach((kartPeer) => {
      this.scoreHigh =
        kartPeer.score > this.scoreHigh ? kartPeer.score : this.scoreHigh;
      this.healthHigh =
        kartPeer.health > this.healthHigh ? kartPeer.health : this.healthHigh;
      this.bulletsHigh =
        kartPeer.totalBullets > this.bulletsHigh
          ? kartPeer.totalBullets
          : this.bulletsHigh;
    });
  }
}
