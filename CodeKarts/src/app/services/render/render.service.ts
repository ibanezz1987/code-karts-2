import { Injectable, NgZone } from '@angular/core';
import * as PIXI from 'pixi.js';
// import { Viewport } from 'pixi-viewport';
import { GameSettingsService } from '../settings/game-settings.service';
import { StatisticsService } from '../statistics/statistics.service';
import { KartPeer } from './../../classes/kart/kart-peer';
import { Layer } from '../../interfaces/layer.type';
import { Viewport } from 'pixi-viewport';

@Injectable({
  providedIn: 'root',
})
export class RenderService {
  app: any;
  private viewport: any;
  private pixiElements: any[] = [];
  private kartContainers: PIXI.Container[] = [];
  private kartSpriteContainers: PIXI.Container[] = [];
  private tires: any[] = [];
  private helmets: PIXI.Sprite[] = [];
  private sights: PIXI.Graphics[] = [];
  private kartNames: PIXI.Container[] = [];
  private defaultKartBars: any = {
    scoreBarBG: [],
    scoreBar: [],
    healthBarBG: [],
    healthBar: [],
    bulletBarBG: [],
    bulletBar: [],
  };
  private kartBars: any[] = [];
  private bullets: PIXI.Container[][] = [];
  private textures: any = {
    baseUrl: 'assets/images/track/',
    textureUrls: {
      grass: 'grass-texture.png',
      water: 'water-texture.png',
      track: 'road-texture.png',
      trackLight: 'road-light-texture.png',
      tree: 'tree.png',
    },
  };
  private waterDisplacementSpriteUrl =
    'assets/images/track/water-displacement-map.png';
  private waterDisplacementSprite!: PIXI.Sprite;
  private waterDisplacementFilter!: PIXI.DisplacementFilter;

  constructor(
    private ngZone: NgZone,
    private settings: GameSettingsService,
    private statistics: StatisticsService
  ) {}

  async createCanvas() {
    /**
     * Make Angular and Pixi get along nicely. See also:
     * https://studiolacosanostra.github.io/2019/02/11/How-to-run-PIXI-in-angular-app/
     */
    await this.ngZone.runOutsideAngular(async () => {
      const applicationOptions: Partial<PIXI.ApplicationOptions> = {
        resizeTo: window,
        antialias: true,
        canvas: document.querySelector('[data-canvas]') as HTMLCanvasElement,
      };
      this.app = new PIXI.Application();
      await this.app.init(applicationOptions);

      this.addWaterDisplacementSprite();
    });
  }

  async addWaterDisplacementSprite() {
    // Add water effect
    // TODO: tmp fix, test Assets loading:
    console.warn('tmp fix, test Assets loading');
    const waterAsset = await PIXI.Assets.load(this.waterDisplacementSpriteUrl);
    this.waterDisplacementSprite = PIXI.Sprite.from(waterAsset);
    this.waterDisplacementFilter = new PIXI.DisplacementFilter(
      this.waterDisplacementSprite
    );
    const textureStyle = new PIXI.TextureStyle({ addressMode: 'repeat' });
    this.waterDisplacementSprite.texture.source.style = textureStyle;
    this.waterDisplacementSprite.scale.x = 4;
    this.waterDisplacementSprite.scale.y = 4;
  }

  animateDisplacementSprites() {
    this.waterDisplacementSprite.x += 2;
    this.waterDisplacementSprite.y += 1;
  }

  renderNewGame() {
    this.createViewport();
    this.drawTrack();
  }

  clearStage(): void {
    // Remove all pixi elements from the stage and memory.
    this.pixiElements.forEach((element) => {
      this.removeElement(element);
    });

    // Empty all previous data
    this.pixiElements = [];
    this.kartContainers = [];
    this.kartSpriteContainers = [];
    this.kartNames = [];
    this.kartBars = [];
    this.bullets = [];

    if (this.viewport) {
      this.viewport.destroy();
    }
  }

  removeElement(element: any): void {
    if (element) element.destroy({ texture: true, baseTexture: true });
  }

  /**
   * Create a viewport with the Viewport plugin. See also:
   * https://davidfig.github.io/pixi-viewport/jsdoc/
   */
  createViewport(): void {
    this.clearStage();

    this.viewport = new Viewport({
      screenWidth: window.innerWidth,
      screenHeight: window.innerHeight,
      worldWidth: this.settings.stage.width,
      worldHeight: this.settings.stage.height,
      events: this.app.renderer.events,
    });

    this.pixiElements.push(this.viewport);

    // Initialize viewport plugins
    this.viewport.drag().pinch().wheel().decelerate().fitWorld();

    this.app.stage.addChild(this.viewport);
  }

  addToViewport(element: any, addToElementList: boolean = true): void {
    this.viewport.addChild(element);

    if (addToElementList) {
      this.pixiElements.push(element);
    }
  }

  drawTrack() {
    this.drawTrackLayers();
    // TODO: setTimeout is a tmp fix:
    console.warn('TODO: remove this tmp fix');
    setTimeout(() => {
      this.drawTrackCheckpoints();
      this.drawTrackElements();
      this.drawPickupZones();
    }, 250);
  }

  drawTrackLayers(): void {
    const layers: Layer[] = this.settings.layers;

    // Draw layers
    layers.forEach((layerInfo, index) => {
      const layer: PIXI.Graphics = new PIXI.Graphics();

      if (layerInfo.borders && layerInfo.borders.length) {
        // Draw according to borders
        layer.moveTo(layerInfo.borders[0].x, layerInfo.borders[0].y);

        layerInfo.borders.forEach((border) => {
          layer.lineTo(border.x, border.y);
        });
        layer.closePath();
      } else {
        // Draw on full stage
        layer
          .setStrokeStyle({ width: 0 })
          .rect(0, 0, this.viewport.worldWidth, this.viewport.worldHeight);
      }

      layer.fill(0x000000);
      this.addToViewport(layer);

      this.renderTrackTexture(layer, layerInfo.type, index);
    });
  }

  async renderTrackTexture(layer: PIXI.Graphics, type: string, index: number) {
    if (this.textures.textureUrls[type]) {
      await PIXI.Assets.load(
        this.textures.baseUrl + this.textures.textureUrls[type]
      );
      const texture = PIXI.Texture.from(
        this.textures.baseUrl + this.textures.textureUrls[type]
      );
      const sprite = new PIXI.TilingSprite({
        texture,
        width: this.settings.stage.width,
        height: this.settings.stage.height,
      });

      if (type === 'water') {
        sprite.addChild(this.waterDisplacementSprite);
        sprite.filters = [this.waterDisplacementFilter];
      }

      sprite.mask = layer;
      sprite.zIndex = index - 100;
      this.addToViewport(sprite);
    }
  }

  drawTrackCheckpoints(): void {
    const checkpoints: any[][] = this.settings.checkpoints;

    checkpoints.forEach((checkpointOrder) => {
      checkpointOrder.forEach((point) => {
        const checkpoint: PIXI.Graphics = new PIXI.Graphics();

        switch (point.type) {
          case 'finish':
            // TODO: add finish variant
            // const texture = PIXI.Texture.from(this.textures.baseUrl + 'finish-line-texture.png');
            // const sprite = new PIXI.TilingSprite(texture, this.settings.stage.width, this.settings.stage.height);

            // sprite.mask = checkpoint;
            checkpoint.setStrokeStyle({
              width: 40,
              color: this.settings.colorFinish,
            });
            checkpoint.moveTo(point.startPoint.x, point.startPoint.y);
            checkpoint.lineTo(point.endPoint.x, point.endPoint.y);
            checkpoint.closePath();

            break;

          // Default checkpoints
          default:
            checkpoint.setStrokeStyle({
              width: 10,
              color: this.settings.colorCheckPoints,
            });
            checkpoint.moveTo(point.startPoint.x, point.startPoint.y);
            checkpoint.lineTo(point.endPoint.x, point.endPoint.y);
            checkpoint.closePath();
        }

        this.addToViewport(checkpoint);
      });
    });
  }

  async drawTrackElements() {
    const elements: any = this.settings.elements;
    // const texture = PIXI.Texture.from(
    //   this.textures.baseUrl + this.textures.textureUrls['tree']
    // );
    const source = this.textures.baseUrl + this.textures.textureUrls['tree'];
    await PIXI.Assets.load(source);
    const texture = new PIXI.Texture({ source });

    // Add trees
    if (elements.trees) {
      elements.trees.forEach((tree: any) => {
        const treeBox: PIXI.Graphics = new PIXI.Graphics();
        const treeSprite = new PIXI.TilingSprite({
          texture,
          width: this.settings.stage.width,
          height: this.settings.stage.height,
        });
        const treeWidthHalved = 360 / 2;

        treeBox.moveTo(tree.x - treeWidthHalved, tree.y - treeWidthHalved);
        treeBox.lineTo(tree.x + treeWidthHalved, tree.y - treeWidthHalved);
        treeBox.lineTo(tree.x + treeWidthHalved, tree.y + treeWidthHalved);
        treeBox.lineTo(tree.x - treeWidthHalved, tree.y + treeWidthHalved);
        treeBox.fill(0x000000);
        treeBox.closePath();

        treeSprite.mask = treeBox;
        treeSprite.x = tree.x - treeWidthHalved;
        treeSprite.y = tree.y - treeWidthHalved;

        this.addToViewport(treeBox);
        this.addToViewport(treeSprite);
      });
    }
  }

  drawPickupZones(): void {
    const zones: any[] = this.settings.trackProjectiles.pickupZones;

    zones.forEach((zone) => {
      this.drawPickupZone(zone.x, zone.y);
    });
  }

  drawPickupZone(x: number, y: number): void {
    // const pickupTypes:any[] = this.settings.trackProjectiles.pickupTypes;
    const circle: PIXI.Graphics = new PIXI.Graphics();

    circle.setStrokeStyle({
      width: 4,
      color: this.settings.colorPickups,
    });
    circle.circle(x, y, this.settings.projectiles.pickupZone.radius);
    circle.fill({ color: this.settings.colorPickups, alpha: 0.1 });

    this.addToViewport(circle);
  }

  async addKart(kartPeer: KartPeer) {
    // Create a new kart container object
    const container = new PIXI.Container();
    const kartContainer = new PIXI.Container();
    // Add containers
    container.addChild(kartContainer);
    this.kartSpriteContainers[kartPeer.id] = kartContainer;
    // Add a sight indicator
    this.addSight(kartPeer);
    // Add sprites
    await this.addKartSprites(kartPeer);
    // Add kart container to stage
    this.addToViewport(container, false);
    this.kartContainers[kartPeer.id] = container;
    // Prepare bars
    this.kartBars[kartPeer.id] = JSON.parse(
      JSON.stringify(this.defaultKartBars)
    );
    // Add a score bar
    this.addScoreBar(container, kartPeer, 240);
    // Add a health bar
    this.addHealthBar(container, kartPeer, 260);
    // Add a bullet bar
    this.addBulletBar(container, kartPeer, 280);
    // Add a bullet array for kart
    this.bullets[kartPeer.id] = [];
    // Set initial kart position
    await this.updateKart(kartPeer);
  }

  async addKartSprites(kartPeer: KartPeer) {
    const kartContainer: PIXI.Container =
      this.kartSpriteContainers[kartPeer.id];
    const assetDirectory: string = 'assets/images/kart/';
    const kartParts: any = {
      kartFramePrimary: 'kartFramePrimary.png',
      kartFrameSecondary: 'kartFrameSecondary.png',
      kartSecondaryBodySeat: 'kartSecondaryBodySeat.png',
      engine: 'engine.png',
      kartSecondaryBodyWing: 'kartSecondaryBodyWing.png',
      kartPrimaryBodySides: 'kartPrimaryBodySides.png',
      kartPrimaryBodyFront: 'kartPrimaryBodyFront.png',
      tireFrontLeft: 'tireFront.png',
      tireFrontRight: 'tireFront.png',
      tireBackLeft: 'tireBack.png',
      tireBackRight: 'tireBack.png',
      driverTertiaryShoes: 'driverTertiaryShoes.png',
      driverSecondaryBody: 'driverSecondaryBody.png',
      kartPrimaryBodySteer: 'kartPrimaryBodySteer.png',
      driverPrimaryHands: 'driverPrimaryHands.png',
      driverPrimaryHelmet: 'driverPrimaryHelmet.png',
    };
    const tires = [
      'tireFrontLeft',
      'tireFrontRight',
      'tireBackLeft',
      'tireBackRight',
    ];
    const ignoreKeysForColoring = ['engine'].concat(tires);

    // Steps for each sprite
    for (var key in kartParts) {
      if (kartParts.hasOwnProperty(key)) {
        // Create sprite
        const part = kartParts[key];

        await PIXI.Assets.load(assetDirectory + part);
        const currentTexture = PIXI.Texture.from(assetDirectory + part);
        const currentSprite = new PIXI.Sprite(currentTexture);
        const ignoreForColoring = ignoreKeysForColoring.indexOf(key) !== -1;

        // Align sprite
        currentSprite.anchor.set(0.5);

        if (tires.indexOf(key) !== -1) {
          // Add tire settings
          this.addTireSettings(kartPeer.id, currentSprite, key);
        } else if (key === 'driverPrimaryHelmet') {
          // Add helmet settings
          this.addHelmetSettings(kartPeer.id, currentSprite);
        }

        // Save sprite
        this.pixiElements.push(currentTexture);
        kartContainer.addChild(currentSprite);

        // Color part if it is given and it is allowed for that part
        if (!ignoreForColoring && kartPeer.colors[key]) {
          currentSprite.tint = kartPeer.colors[key];
        }
      }
    }
  }

  addTireSettings(
    kartPeerId: number,
    currentSprite: PIXI.Sprite,
    key: string
  ): void {
    const tireData = this.tires[kartPeerId] ? this.tires[kartPeerId] : {};

    switch (key) {
      case 'tireFrontLeft':
        currentSprite.x = -94;
        currentSprite.y = -69;
        tireData.tireFrontLeft = currentSprite;
        break;

      case 'tireFrontRight':
        currentSprite.x = 94;
        currentSprite.y = -69;
        tireData.tireFrontRight = currentSprite;
        break;

      case 'tireBackRight':
        currentSprite.x = -104;
        currentSprite.y = 103;
        break;

      case 'tireBackLeft':
        currentSprite.x = 104;
        currentSprite.y = 103;
        break;
    }

    this.tires[kartPeerId] = tireData;
  }

  addHelmetSettings(kartPeerId: number, currentSprite: PIXI.Sprite): void {
    currentSprite.y = 80;
    this.helmets[kartPeerId] = currentSprite;
  }

  updateTrack(): void {
    this.animateDisplacementSprites();
  }

  async updateKart(kartPeer: KartPeer) {
    // Search for existing kart container
    const container = this.kartContainers[kartPeer.id];
    const spriteContainer = this.kartSpriteContainers[kartPeer.id];
    // Update kart position
    container.x = kartPeer.position.x;
    container.y = kartPeer.position.y;
    spriteContainer.angle = kartPeer.rotation;
    // Update the score bar
    const scorePercentage: number = Math.round(
      this.statistics.scoreHigh > 0
        ? (100 / this.statistics.scoreHigh) * kartPeer.score
        : 0
    );
    this.kartBars[kartPeer.id].scoreBar.width = scorePercentage;
    this.kartBars[kartPeer.id].scoreBar.x = scorePercentage / 2 - 50;
    // Update the health bar
    const healthPercentage: number = Math.round(
      (100 / this.settings.kart.initialHealth) * kartPeer.health
    );
    this.kartBars[kartPeer.id].healthBar.width = healthPercentage;
    this.kartBars[kartPeer.id].healthBar.x = healthPercentage / 2 - 50; // - 50 is the place where the bars are originally aligned
    // Update the bullet bar
    const bulletPercentage: number = Math.round(
      (100 / this.settings.projectiles.pickupZone.bullet.max) *
        kartPeer.totalBullets
    );
    this.kartBars[kartPeer.id].bulletBar.x = bulletPercentage / 2 - 50;
    this.kartBars[kartPeer.id].bulletBar.width = bulletPercentage;
    this.updateTires(kartPeer);
    this.updateBullets(kartPeer);
    await this.updateKartName(container, kartPeer);
  }

  updateDeadKart(kartPeer: KartPeer): void {
    // Show dead status of kart
    this.kartContainers[kartPeer.id].alpha = 0.1;
    this.kartBars[kartPeer.id].scoreBarBG.alpha = 0;
    this.kartBars[kartPeer.id].scoreBar.alpha = 0;
    this.kartBars[kartPeer.id].healthBarBG.alpha = 0;
    this.kartBars[kartPeer.id].healthBar.alpha = 0;
    this.kartBars[kartPeer.id].bulletBarBG.alpha = 0;
    this.kartBars[kartPeer.id].bulletBar.alpha = 0;
  }

  addScoreBar(container: PIXI.Container, kartPeer: KartPeer, y: number): void {
    // Add score bar background
    const scoreBarBG = new PIXI.Graphics();
    scoreBarBG.y = y;
    scoreBarBG.setStrokeStyle({ width: 0, color: 0x000000 });
    scoreBarBG.rect(-50, 0, 100, 10);
    scoreBarBG.fill(this.settings.colorScoreBG);
    container.addChild(scoreBarBG);
    this.pixiElements.push(scoreBarBG);

    this.kartBars[kartPeer.id].scoreBarBG = scoreBarBG;

    // Add score bar
    const scoreBar = new PIXI.Graphics();
    scoreBar.y = y;
    scoreBar.setStrokeStyle({ width: 0, color: 0x000000 });
    scoreBar.rect(-50, 0, 100, 10);
    scoreBar.fill(this.settings.colorScore);
    container.addChild(scoreBar);
    this.pixiElements.push(scoreBar);

    this.kartBars[kartPeer.id].scoreBar = scoreBar;
  }

  addHealthBar(container: PIXI.Container, kartPeer: KartPeer, y: number): void {
    // Add health bar background
    const healthBarBG = new PIXI.Graphics();
    healthBarBG.y = y;
    healthBarBG.setStrokeStyle({ width: 0, color: 0x000000 });
    healthBarBG.rect(-50, 0, 100, 10);
    healthBarBG.fill(this.settings.colorHealthBG);
    container.addChild(healthBarBG);
    this.pixiElements.push(healthBarBG);

    this.kartBars[kartPeer.id].healthBarBG = healthBarBG;

    // Add health bar
    const healthBar = new PIXI.Graphics();
    healthBar.y = y;
    healthBar.setStrokeStyle({ width: 0, color: 0x000000 });
    healthBar.rect(-50, 0, 100, 10);
    healthBar.fill(this.settings.colorHealth);
    container.addChild(healthBar);
    this.pixiElements.push(healthBar);

    this.kartBars[kartPeer.id].healthBar = healthBar;
  }

  addBulletBar(container: PIXI.Container, kartPeer: KartPeer, y: number): void {
    // Add score bar background
    const bulletBarBG = new PIXI.Graphics();
    bulletBarBG.y = y;
    bulletBarBG.setStrokeStyle({ width: 0, color: 0x000000 });
    bulletBarBG.rect(-50, 0, 100, 10);
    bulletBarBG.fill(this.settings.colorPickupsBG);
    container.addChild(bulletBarBG);
    this.pixiElements.push(bulletBarBG);

    this.kartBars[kartPeer.id].bulletBarBG = bulletBarBG;

    // Add score bar
    const bulletBar = new PIXI.Graphics();
    bulletBar.y = y;
    bulletBar.setStrokeStyle({ width: 0, color: 0x000000 });
    bulletBar.rect(-50, 0, 100, 10);
    bulletBar.fill(this.settings.colorPickups);
    container.addChild(bulletBar);
    this.pixiElements.push(bulletBar);

    this.kartBars[kartPeer.id].bulletBar = bulletBar;
  }

  async updateKartName(container: PIXI.Container, kartPeer: KartPeer) {
    let nameContainer: PIXI.Container = this.kartNames[kartPeer.id];

    if (!nameContainer) {
      // Add new name
      nameContainer = await this.addKartName(kartPeer);
      container.addChild(nameContainer);
    }

    // Update position
    nameContainer.y = this.settings.kart.radius + 50;
  }

  addKartName(kartPeer: KartPeer): PIXI.Container {
    const container = new PIXI.Container();
    const name = `${kartPeer.name} #${kartPeer.id}`;
    const text = new PIXI.Text({
      text: name,
      style: {
        fontFamily: 'Arial',
        fontSize: 24,
        fill: 0xeeeeee,
        align: 'center',
      },
    });

    text.anchor.set(0.5);
    container.addChild(text);

    this.pixiElements.push(container);
    this.kartNames[kartPeer.id] = container;

    return container;
  }

  addSight(kartPeer: KartPeer): void {
    const kartContainer: PIXI.Container =
      this.kartSpriteContainers[kartPeer.id];
    const indicator: PIXI.Graphics = new PIXI.Graphics();
    const indicatorWidth: number = 6;

    indicator.rect(
      -0.5 * indicatorWidth,
      -this.settings.kart.viewDistance,
      indicatorWidth,
      this.settings.kart.viewDistance
    );
    indicator.fill(0xffffff);
    indicator.alpha = 0.05;

    this.sights[kartPeer.id] = indicator;
    kartContainer.addChild(indicator);
  }

  updateTires(kartPeer: KartPeer): void {
    const kartTires: any = this.tires[kartPeer.id];
    const leftTireSprite: PIXI.Sprite = kartTires.tireFrontLeft;
    const rightTireSprite: PIXI.Sprite = kartTires.tireFrontRight;

    leftTireSprite.angle = kartPeer.tireRotation;
    rightTireSprite.angle = kartPeer.tireRotation;
  }

  updateBullets(kartPeer: KartPeer): void {
    const kartBullets = this.bullets[kartPeer.id];

    if (kartBullets.length < kartPeer.bullets.length) {
      // Add new bullets
      const bulletGraphics = this.getNewBullet();

      kartBullets.push(bulletGraphics);
    }

    // Remove redundant bullets
    while (kartBullets.length > kartPeer.bullets.length) {
      this.removeElement(kartBullets[0]);
      kartBullets.shift();
    }

    // Update position
    for (let i = 0; i < kartBullets.length; i++) {
      const bulletGraphics = kartBullets[i];

      bulletGraphics.x = kartPeer.bullets[i].position.x;
      bulletGraphics.y = kartPeer.bullets[i].position.y;
    }
  }

  getNewBullet(): PIXI.Container {
    const container = new PIXI.Container();
    const graphics = new PIXI.Graphics();

    graphics.setStrokeStyle({ width: 1, color: 0xffffff });
    graphics.circle(0, 0, this.settings.projectiles.bullet.radius);
    graphics.fill(0xffffff);
    container.addChild(graphics);
    this.addToViewport(container, false);

    return container;
  }
}
