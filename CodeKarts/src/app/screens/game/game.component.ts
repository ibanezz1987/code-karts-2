import { Component } from '@angular/core';
import { GameService } from '../../services/game/game.service';
import { CanvasComponent } from '../../canvas/canvas.component';

@Component({
  selector: 'app-game',
  // Refactor for update from angular 8 to 19
  imports: [CanvasComponent],
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss'],
})
export class GameComponent {
  constructor(private gameS: GameService) {}

  ngAfterViewInit(): void {
    // Automatically start a new game after after tempalte is loaded
    // TODO: tmp fix:
    setTimeout(() => {
      console.warn('TODO: remove this tmp fix');
      this.gameS.startNewGame();
    }, 1000);
  }
}
